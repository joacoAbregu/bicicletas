var express = require('express');
var router = express.Router();
var usuariosController = require('../controllers/usuarios');

router.get('/', usuariosController.usuario_list);
router.get('/create', usuariosController.usuario_create_get);
router.post('/create', usuariosController.usuario_create_post);
router.get('/:id/update', usuariosController.usuario_update_get);
router.post('/:id/update', usuariosController.usuario_update_post);
router.post('/:id/delete', usuariosController.usuario_delete_post);

module.exports = router;
