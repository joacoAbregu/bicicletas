var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

var base_url = 'http://localhost:3000/api/bicicletas';


describe('API Bicicleta', function () {
    beforeAll(function(done) {
        mongoose.connection.close().then(() => {
            var mongoDB = 'mongodb://localhost/testdb';
            mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
            mongoose.set('useCreateIndex', true);
    
            var db = mongoose.connection;
            db.on('error', console.error.bind(console, 'MongoDB connection error: '));
            db.once('open', function () {
                console.log('We are connected to test database!');
                done();
            });

        });

    });



    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, sccuess){
           if (err) console.log(err);
           done(); 
        });
    });

    describe('Get Bicicletas', () => {
        it('Status 200', (done) => {
            request.get(base_url, function(error, response, body){
                
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);           
                done();
            });

        });
    });

    describe('Bicicletas /create', () => {
        it('Status 200', (done) => {
            var headers = {'content-type': 'application/json'};
            var aBici = '{ "code": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54}';

            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                
                var bici = JSON.parse(body).bicicleta;

                expect(bici.color).toBe('rojo');
                expect(bici.ubicacion[0]).toBe(-34);
                expect(bici.ubicacion[1]).toBe(-54);

                done();
            });

        });
    });

    describe('Bicicletas /update', () => {
        it('Status 200', (done) => {
            var headers = {'content-type': 'application/json'};
            var bici = '{"code": 2, "color": "azul", "modelo": "urbana", "lat": -34, "lng": -54 }';

            var aBici = new Bicicleta({
                code: 2,
                color: 'rojo',
                modelo: 'montaña',
                ubicacion: [-34,-54]
            });
        
            Bicicleta.add(aBici, function (err, newBici) {
                request.post({
                    headers: headers,
                    url: base_url + `/update`,
                    body: bici
                }, function(err, resp, body) {
                    expect(resp.statusCode).toBe(200);
                    
                    Bicicleta.findByCode(aBici.code, function (err, targetBici) {
                        console.log(targetBici);

                        expect(targetBici.color).toBe('azul');
                        expect(targetBici.modelo).toBe('urbana');
                        expect(targetBici.ubicacion[0]).toBe(-34);
                        expect(targetBici.ubicacion[1]).toBe(-54);
                        
                        done();
                    });
                }); 
            });
        });
    });

    describe('Bicicletas /delete', () => {
        it('Status 204', (done) => {
            var aBici = Bicicleta.createInstance(2, 'negra', 'urbana', [-34,-54]);
            
            Bicicleta.add(aBici, function (err, newBici) {
                var headers = {'content-type': 'application/json'};
                var bici = '{"code": 2}';
                
                request.delete({
                    headers: headers,
                    url: base_url + '/delete',
                    body: bici
                }, function(err, resp, body) {
                    expect(resp.statusCode).toBe(204);
                    
                    Bicicleta.allBicis(function (err, newBicis) {
                        expect(newBicis.length).toBe(0);

                        done();
                    });
                });
            });
        });
    });


});
