var mymap = L.map('main_map').setView([-34.586575, -58.399121], 13);

L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
}).addTo(mymap)



$.ajax({
    method: 'POST',
    dataType: 'json',
    url: 'api/auth/authenticate',
    data: { email: 'natori6@maltacp.com', password: 'demo123' },
}).done(function( data ) {
    console.log(data);

    $.ajax({
        dataType: 'json',
        url: 'api/bicicletas',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("x-access-token", data.data.token);
        }
    }).done(function (result) {
        console.log(result);

        result.bicicletas.forEach(bici => {
            L.marker(bici.ubicacion, { title: bici.id }).addTo(mymap);
        });
    });
});