var mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
var Reserva = require('./reserva');
const bcrypt = require('bcrypt');
let crypto = require('crypto');
const mailer = require('../mailer/mailer');
const Token = require('../models/token');

const saltRounds = 10;
var Schema = mongoose.Schema;

const validateEmail = function(email) {
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email);
};

var usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        lowercase: true,
        unique: true,
        validate: [validateEmail, 'El email no es valido'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
    },
    password: {
        type: String,
        trim: true,
        required: [true, 'El password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    },
    googleId: String,
    facebookId: String
});

usuarioSchema.plugin(uniqueValidator, { message: 'El {PATH} ya existe con otro usuario' });

usuarioSchema.pre('save', function(next){
    if(this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, saltRounds)
    }
    next();
});

usuarioSchema.methods.validPassword = function(password){
    return bcrypt.compareSync(password, this.password)
};

usuarioSchema.methods.reservar = function (biciID, desde, hasta, cb) {
    var reserva = new Reserva({
        desde: desde,
        hasta: hasta,
        bicicleta: biciID,
        usuario: this._id
    });

    console.log(reserva);
    reserva.save(cb);
}

usuarioSchema.methods.enviar_email_bienvenida = function (cb) {
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex') });
    const email_destination = this.email;

    token.save(function (err) {
        if (err) {
            console.log(err.message);
        }

        const mailOptions = {
            from: 'no-reply@bicicletas.com',
            to: email_destination,
            subject: 'Verificación de cuenta',
            text: 'Hola,\n\n' + 'Por favor, para verificar su cuenta haga click en el siguiente link:\n\n' + 'http://localhost:3000' + '\/token/confirmation\/' + token.token + ' .\n'
        };

        mailer.sendMail(mailOptions, function (err) {
            if (err) {
                console.log(err.message);
            }

            console.log('Se ha enviado un email de verificación a ' + email_destination + '.');
        });
    });
}

usuarioSchema.methods.resetPassword = function (cb) {
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex') });
    const email_destination = this.email;

    token.save(function (err) {
        if (err) {
            return cb(err);
        }

        const mailOptions = {
            from: 'no-reply@bicicletas.com',
            to: email_destination,
            subject: 'Reseteo de Password de Cuenta',
            text: 'Hola,\n\n' + 'Por favor, para resetear el password de su cuenta haga click en este link:\n\n' +
            'http://localhost:3000' + '\/resetPassword\/' + token.token 
            
        };

        mailer.sendMail(mailOptions, function (err) {
            if (err) {
                return cb(err);
            }

            console.log('Se envió un email para restablecer contraseña a ' + email_destination + '.');
        });

        cb(null);
    });
}

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition, callback) {
    const self = this;
    console.log(condition);

    this.findOne({
        $or: [
            { 'googleId': condition.id },
            { 'email': condition.emails[0].value }
        ]
    }, 
    (err, result) => {
        if (result) {
            callback(err, result);
        } else {
            let values = {};
            console.log('=============== CONDITION ===============');
            console.log(condition);

            values.googleId = condition.id;
            values.email = condition.emails[0].value;
            values.nombre = condition.displayName || 'SIN NOMBRE';
            values.verificado = true;
            values.password = crypto.randomBytes(16).toString('hex');

            console.log('=============== VALUES ===============');
            console.log(values);

            self.create(values, function (err, result) {
                if (err) {
                    console.log(err);
                }

                return callback(err, result);
            });
        }
    });
}

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition, callback) {
    const self = this;
    console.log(condition);

    this.findOne({
        $or: [
            { 'facebookId': condition.id },
            { 'email': condition.emails[0].value }
        ]
    }, 
    (err, result) => {
        if (result) {
            callback(err, result);
        } else {
            let values = {};
            console.log('=============== CONDITION ===============');
            console.log(condition);

            values.facebookId = condition.id;
            values.email = condition.emails[0].value;
            values.nombre = condition.displayName || 'SIN NOMBRE';
            values.verificado = true;
            values.password = crypto.randomBytes(16).toString('hex');

            console.log('=============== VALUES ===============');
            console.log(values);

            self.create(values, function (err, user) {
                if (err) {
                    console.log(err);
                }

                return callback(err, user);
            });
        }
    });
}


module.exports = mongoose.model('Usuario', usuarioSchema);